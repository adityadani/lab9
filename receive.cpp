#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>           // close()
#include <string.h>           // strcpy, memset()

#include <netinet/ip.h>       // IP_MAXPACKET (65535)
#include <sys/types.h>        // needed for socket(), uint8_t, uint16_t
#include <sys/socket.h>       // needed for socket()
#include <linux/if_ether.h>   // ETH_P_ARP = 0x0806, ETH_P_ALL = 0x0003
#include <net/ethernet.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <sys/ioctl.h>

#include <netinet/in.h>
#include <net/if.h>
#include <net/if_arp.h>
#include <linux/if_packet.h>

#include <netinet/ip.h>
#include <netinet/tcp.h>
#include <netinet/ether.h>
#include <netinet/ip_icmp.h>

#include <arpa/inet.h>
#include <errno.h>            // errno, perror()

struct ifreq Interface1;         
struct sockaddr_ll sll;
struct ifreq ifr; 
int sd, status;

#define IF_NAME "eth0"

static void get_ip_address_index(struct ifreq *if_idx, char *interface) {
	int fd;
	fd = socket(AF_INET, SOCK_DGRAM, 0);
	memset(if_idx, 0, sizeof(struct ifreq));
	//if_idx->ifr_addr.sa_family = AF_INET;
	strncpy(if_idx->ifr_name, interface, IFNAMSIZ-1);
	if (ioctl(fd, SIOCGIFINDEX, if_idx) < 0)
		perror("SIOCGIFINDEX");
	close(fd);	
}


void recv_packet(int packet_size, char *pkt) {
	socklen_t len;
	len = sizeof(struct sockaddr_ll);
	if ((status = recvfrom (sd, pkt, packet_size, 0,(struct sockaddr *)&sll,&len)) < 0) {
		perror ("recv() failed:");
		exit (EXIT_FAILURE);
	}
}

void raw_socket_recv_setup() {
	struct ifreq if_idx_1;
	char interface1[20];

	strcpy(interface1, IF_NAME);
	// Submit request for a raw socket descriptor.
	if ((sd = socket (AF_PACKET, SOCK_RAW, htons(ETH_P_ALL))) < 0) { // htons(ETH_P_ALL)
		perror ("socket() failed ");
		exit (EXIT_FAILURE);

	}          
	
	int res, sendbuff = 300000;

 	printf("sets the send buffer to %d\n", sendbuff);


	//res = setsockopt(sd, SOL_SOCKET, SO_RCVBUF, &sendbuff, sizeof(sendbuff));
 	//if(res == -1)
     	//	printf("Error setsockopt");	
	//memset(&Interface1, 0, sizeof(Interface1));
	//strncpy(Interface1.ifr_ifrn.ifrn_name, "eth0", IFNAMSIZ);     
	//if (setsockopt(sd, SOL_SOCKET, SO_BINDTODEVICE, &Interface1, sizeof(Interface1)) < 0) { close(sd); }
	
	bzero(&sll , sizeof(sll));

	get_ip_address_index(&if_idx_1, interface1);
	sll.sll_family = AF_PACKET; 
	sll.sll_ifindex = if_idx_1.ifr_ifindex; 
	sll.sll_protocol = htons(ETH_P_ALL); 
	
	printf("\n Bind");
	if((bind(sd , (struct sockaddr *)&sll , sizeof(sll))) ==-1) {
		perror("bind: ");
		exit(-1);
	}
	printf("\n Bind done");
}

void close_raw_socket() {
	close(sd);
}
