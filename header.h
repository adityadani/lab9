#include <sys/types.h>
#define MAX_PACKET_SIZE 1500
#define MIN_PACKET_SIZE 100
#define WINDOW_SIZE 254 

#define ACK 0
#define SEQ 1
#define MY_ETHER_TYPE 0x7777

typedef struct tagUDP {
	u_char d_host_ether[6];
	u_int16_t s_port;
	u_int16_t d_port;
	u_int16_t check;
	u_int8_t s_ip: 6;
	u_int8_t protocol_type: 1; // TCP or UDP
	u_int8_t padd1: 1;
	u_int8_t d_ip: 6;
	u_int8_t padd2: 2;
} udphdr;

typedef struct tagTCP{
	u_char d_host_ether[6];
	u_int16_t s_port;
	u_int16_t d_port;
	u_int16_t check;
	u_int8_t seq_no;
	u_int8_t s_ip: 6;
	u_int8_t protocol_type: 1; // TCP or UDP
	u_int8_t last_packet: 1;
	u_int8_t d_ip: 6;
	u_int8_t packet_type: 1; // Seq or ACK
	u_int8_t toggle_window: 1;
} tcphdr;

#define UDP_HEADER_SIZE sizeof(udphdr)
#define TCP_HEADER_SIZE sizeof(tcphdr)
