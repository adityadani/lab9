#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <pcap.h>

// extern int build_routing_table();

// std::queue<QueueEntry> pkt_queue;


void got_packet(u_char *args, const struct pcap_pkthdr *header, const u_char *packet)
{

	printf("\nGot the packet with len %d \nData: ",header->caplen);
	
	int i=0;
	for(i=0;i<135;i++)
	printf("%x",packet[i]);
	
	
	//check if it is a udp packet or a tcp packet
	//if udp // 0 for tcp , 1 for udp
	if(udp_h->protocol_type == 1) handle_udp(packet);
	else handle_tcp();
	return;
}


int main(int argc, char *argv[])
{
	pcap_t *handle;			
	char dev[20], errbuf[PCAP_ERRBUF_SIZE];	
	struct bpf_program fp;		
	bpf_u_int32 mask;
	bpf_u_int32 net;		
	struct pcap_pkthdr header;	
	const u_char *packet;		
	int num_packets=5;

// 	build_routing_table();
	
	strcpy(dev,"eth0");
	if (dev == NULL) {
		fprintf(stderr, "error in finding default device: %s\n", errbuf);
		return(2);
	}
	
	if (pcap_lookupnet(dev, &net, &mask, errbuf) == -1) {
		fprintf(stderr, "error in getting netmask for device %s: %s\n", dev, errbuf);
		net = 0;
		mask = 0;
	}
	
	handle = pcap_open_live(dev, BUFSIZ, 0, 1000, errbuf);
	if (handle == NULL) {
		fprintf(stderr, "error in opening the device %s: %s\n", dev, errbuf);
		return(2);
	}
	
	/* Grab a packet */
	pcap_loop(handle, num_packets, got_packet, NULL);
	pcap_close(handle);
// 	send_on_raw(argv);
	return(0);
}
