iperf_app: iperf.o packet.o send_packet.o receive.o common.o
	g++ -o iperf_app -g iperf.o packet.o send_packet.o receive.o common.o

receive: receive.cpp
	g++ -g -c -Wall receive.cpp

send_packet: send_packet.cpp
	g++ -g -c -Wall send_packet.cpp

iperf: iperf.cpp
	g++ -g -c -Wall iperf.cpp

common: common.cpp
	g++ -g -c -Wall common.cpp

clean: 
	rm -rf *.o iperf_app
