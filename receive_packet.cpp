/**************************** header files ******************************/
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>

#include <arpa/inet.h>
#include <linux/if_packet.h>
#include <linux/ip.h>
#include <linux/udp.h>

#include <sys/ioctl.h>
#include <sys/socket.h>
#include <net/if.h>
#include <netinet/in.h>
#include <netinet/ether.h>

/**************************** macros ******************************/

#define BUFFER_SIZE 1024
#define IF_NAME "eth0"			// interface_name
#define ETHER_TYPE 0x0800       // ipv4

/**************************** main function ******************************/

int main (int argc, char* argv[])
{
	int sockfd, ret=0, sockopt;
	struct ifreq ifopts;
	struct ifreq if_ip;
	struct sockaddr_storage s_addr;
	uint8_t buffer[BUFFER_SIZE];
	char if_name[IFNAMSIZ];
	struct ether_header *eh;
	struct iphdr *iph;
	struct udphdr *udph;
	long unsigned numbytes;
	
	memset (&if_ip, 0, sizeof (struct ifreq));
	strcpy(if_name, IF_NAME);        //  take interface name
	
	eh = (struct ether_header *) buffer;
	iph = (struct iphdr *) (buffer+ sizeof (struct ether_header));
	udph = (struct udphdr *) (buffer + sizeof (struct ether_header) + sizeof (struct iphdr));
	
	if ((sockfd = socket (PF_PACKET, SOCK_RAW, htons (ETHER_TYPE))) == -1)
	//if ((sockfd = socket (PF_PACKET, SOCK_RAW, htons (ETH_P_ALL))) == -1)
	{
		perror ("socket listener error");
		return 0;
	}

	strncpy(ifopts.ifr_name, if_name, IFNAMSIZ-1);
	ioctl(sockfd, SIOCGIFFLAGS, &ifopts);
// 	ifopts.ifr_flags |= IFF_PROMISC;
	ioctl(sockfd, SIOCSIFFLAGS, &ifopts);
	
	if ( setsockopt(sockfd, SOL_SOCKET, SO_REUSEADDR, &sockopt, sizeof (sockopt)) == -1)
	{
		perror("setsockopt error");
		close(sockfd);
		exit(EXIT_FAILURE);
	}
	
	if (setsockopt(sockfd, SOL_SOCKET, SO_BINDTODEVICE, if_name, IFNAMSIZ-1) == -1)
	{
		perror("Binding to device error");
		close(sockfd);
		exit(EXIT_FAILURE);
	}
	
	int j=0,i;
	while(j<5)
	{
		printf ("\nwaiting to receive a packet");
		numbytes = recvfrom(sockfd, buffer, BUFFER_SIZE, 0, NULL, NULL);
		printf("\nreceived packet of bytes: %lu",numbytes);
		printf("\tData:");
		for (i=0; i<numbytes; i++) printf("%02x:", buffer[i]);
		printf("\n");
		
		j++;
	}
}